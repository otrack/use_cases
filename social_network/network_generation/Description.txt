The notebook "Network Generation with Chi2-distributed degree distributions.ipynb" generates a network graph where the node degrees follow a Chi^2 distribution:
- "Network_Graph.gpickle" -> final graph
- "Network_Graph_intermediary.gpickle" -> Graph before the rewiring procedure

Hyperparameters (14): 
- (1) number of users
- (3) rank correlation between reciprocal, in- and out-degree
- (9) three parameters for the Chi^2 distributions (degrees of freedom, location and scale) 
- (1) percentile of nodes for the rewiring procedure 

The notebook "Network Generation flexible degree distributions.ipynb" generates a network graph where the node degree distributions can be chosen arbitrarily.

The notebook "Graph Analysis.ipynb" analyses the generated graph by returning topological graph features and several plots.

The network graph can also be displayed, but the computation can take quite a while and the resulting graphs are often not clear, especially for big graphs.

The following libraries need to be installed for the execution of the notebooks:
Python version 3.8.5
conda install -c anaconda numpy
conda install -c anaconda pandas
conda install -c conda-forge matplotlib
conda install -c anaconda networkx
conda install -c trentonoliphant datetime
conda install -c anaconda scipy

A network graph, created with Chi^2 distributions, with the following hyperparameters is uploaded:
- number of nodes: 500
- Rank correlation (reciprocal,in) = 0.8
- Rank correlation (reciprocal,out) = 0.8
- Rank correlation (in,out) = 0.8
- Chi^2(reciprocal): df=0.5, loc=0.49, scale=10
- Chi^2(in): df=0.8, loc=0.49, scale=10
- Chi^2(out): df=0.1, loc=0.49, scale=80
- Rewire node percentile: 0.9

Further hyperparameter configurations for the crawled network graphs as well as their topological features can be found in the file "Hyperparameters.doc".
